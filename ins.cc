#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

using std::pair;
using std::vector;
using std::map;
using std::cout;
using std::cin;
using std::endl;

int main(void){
    int n;
    vector<int> array;

    cin >> n;
    for(int i=0;i<n;i++){
        int a;
        cin >> a;
        array.push_back(a);
    }

    int swaps = 0;
    for(int i=1;i<n;i++){
        int k=i;
        while(k > 0 && array[k] < array[k-1]){
            int t = array[k];
            array[k] = array[k-1];
            array[k-1] = t;
            swaps++;

            k--;
        }
    }

    cout << swaps << endl;

    return 0;
}
