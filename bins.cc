#include <iostream>
#include <vector>
using namespace std;

int find_index(const vector<int>& arr, int k){
    unsigned int begin,end;

    begin = 0;
    end = arr.size()-1;
    while(end - begin > 1){
        int mid = (begin+end)/2;

        if(k == arr[mid]) return mid;
        else if(k < arr[mid]){
            end = mid-1;
        } else {
            begin = mid+1;
        }
    }

    while(begin < arr.size() && arr[begin] < k) begin++;
    if(arr[begin] == k) return begin;

    return -1;
}

int main(void){
    int n,m;
    vector<int> arr;

    cin >> n >> m;
    for(int i=0;i<n;i++){
        int a;
        cin >> a;
        arr.push_back(a);
    }

    for(int i=0;i<m;i++){
        int k,j;

        cin >> k;
        j = find_index(arr,k);
        cout << (j == -1 ? -1 : j+1);

        if(i != m-1) cout << " ";
    }
    cout << endl;

    return 0;
}
