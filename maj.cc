#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

using std::map;
using std::vector;
using std::cin;
using std::cout;
using std::endl;

int naive_method(const vector<int>& array){
    map<int,int> count;

    for(auto it=array.begin(); array.end() != it; it++){
        int a = *it;
        count[a]++; // www.cplusplus.com/reference/map/map/operator[]/
    }

    int elem = -1;
    for(auto it=count.begin(); count.end() != it; it++){
        int a,c;
        a = it->first;
        c = it->second;
        if(c > array.size()/2){
            elem = a;
            break;
        }
    }
    return elem;
}

int boyer_moore(const vector<int>& array){
    int elem,cc;

    cc = 0, elem = -1;
    for(auto it=array.begin(); array.end() != it; it++){
        int a = *it;

        if(cc == 0){
            cc = 1;
            elem = a;
        } else {
            if(elem == a){
                cc++;
            } else {
                cc--;
            }
        }
    }

    int count = std::count(array.begin(), array.end(), elem);
    
    if(count > array.size()/2) return elem;
    return -1;
}

int main(void){
    int k,n;

    cin >> k >> n;
    for(int _i=0;_i<k;_i++){
        vector<int> array;

        for(int _j=0;_j<n;_j++){
            int a;
            cin >> a;
            array.push_back(a);
        }

        //cout << naive_method(array) << " ";
        cout << boyer_moore(array) << " ";
    }
    cout << endl;

    return 0;
}
