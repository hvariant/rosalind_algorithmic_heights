#include <iostream>
using namespace std;

int fib(int i){
    if(i <= 1) return i;

    return fib(i-1) + fib(i-2);
}

int main(void){
    int n;
    
    cin >> n;
    cout << fib(n) << endl;

    return 0;
}
