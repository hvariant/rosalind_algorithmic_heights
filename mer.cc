#include <iostream>
#include <list>

using namespace std;

int main(void){
    int n1,n2;
    list<int> arr1, arr2, out;

    cin >> n1;
    for(int i=0;i<n1;i++){
        int x;
        cin >> x;
        arr1.push_back(x);
    }
    cin >> n2;
    for(int i=0;i<n2;i++){
        int x;
        cin >> x;
        arr2.push_back(x);
    }

    while(arr1.size() > 0 && arr2.size() > 0){
        int f1 = arr1.front(), f2 = arr2.front();
        if(f1 < f2){
            out.push_back(f1);
            arr1.pop_front();
        } else {
            out.push_back(f2);
            arr2.pop_front();
        }
    }
    while(arr1.size() > 0) out.push_back(arr1.front()), arr1.pop_front();
    while(arr2.size() > 0) out.push_back(arr2.front()), arr2.pop_front();

    for(auto it=out.begin(); it != out.end(); it++){
        cout << *it << " ";
    }
    cout << endl;

    return 0;
}
