#include <iostream>
#include <vector>
using namespace std;

int main(void){
    int n,m;

    cin >> n >> m;
    // initialize neighbours
    // http://stackoverflow.com/questions/21663256/how-to-initialize-a-vector-of-vectors
    vector<vector<int> > neighbours(n+1, vector<int>());
    for(int _i=0;_i<m;_i++){
        int u,v;
        cin >> u >> v;
        neighbours[u].push_back(v);
        neighbours[v].push_back(u);
    }

    for(int i=1;i<=n;i++){
        cout << neighbours[i].size() << " ";
    }
    cout << endl;

    return 0;
}
